function unixtimeToDate(unixtime) {
  const date = new Date(unixtime * 1000);
  return Utilities.formatDate(date, 'Asia/Tokyo', 'yyyy/MM/dd');
}

function unixtimeToTime(unixtime) {
  const time = new Date(unixtime * 1000);
  return Utilities.formatDate(time, 'Asia/Tokyo', "HH:mm");
}

function returnIcon(icon) {
  switch (icon) {
    case '01n':
    case '01d':
      return '☀️';
    case '02n':
    case '02d':
      return '⛅️';
    case '03n':
    case '03d':
    case '04n':
    case '04d':
      return '☁️';
    case '09n':
    case '09d':
    case '10n':
    case '10d':
      return '☂️';
    case '11n':
    case '11d':
      return '🌩';
    case '13n':
    case '13d':
      return '☃️';
    case '50n':
    case '50d':
      return '🌫';
    default:
      return "";
  }
}

function todayForecastAnnounce() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?id={ (1) City ID }&appid={ (2) Your API Key }&lang=ja";
  var response = UrlFetchApp.fetch(url);
  var json = JSON.parse(response.getContentText());

  var forecast = "";

  forecast += "おはようございます!\n本日の天候情報をお知らせします!\n\n";

  forecast += "             *" + json.city.name + ": " + unixtimeToDate(json.list[0].dt) + "*\n";

  for (var i = 0; i < 5; i++) {
    forecast += "-----------------------------------------\n";
    forecast += "時刻: *" + unixtimeToTime(json.list[i].dt) + "*\n";
    forecast += "天候: " + json.list[i].weather[0].description + returnIcon(json.list[i].weather[0].icon) + "\n";
    forecast += "気温: " + Math.floor(json.list[i].main.temp - 273.15) + " (℃)\n";
    if (json.list[i].rain) {
      var str = JSON.stringify(json.list[i].rain);
      var str_slice = str.slice(6);
      var rain_3h = str_slice.slice(0, -1);
      forecast += "降水量: " + rain_3h + " (mm/3h)\n";
    } else {
      forecast += "降水量: 0.0 (mm/3h)\n";
    }
  }

  var data =
  {
    "type": "section",
    "text": forecast
  };

  var options =
  {
    "method": "POST",
    'contentType': 'application/json',
    'payload': JSON.stringify(data),
  };
  UrlFetchApp.fetch("{ (3) Your Incoming Webhook URL }", options);
}

function myFunction() {
  var date = new Date();
  var day = date.getDay();

  if (day === 0 || day === 6) {
    return;
  } else {
    todayForecastAnnounce();
  }
}
